class CarAlarmSystem {
    var open = false
    val closed get() = !open
    var locked = false
    val unlocked get() = !locked
    var flash = false
    var sound = false
    var armed = false
    var clock = 0L

    fun lock() {
        if (closed) {
            locked = true
        }
    }

    fun unlock() {
        clock = 0
        if (armed) {
            armed = false
            flash = false
            sound = false
        }
    }

    fun close() {
        open = false
    }

    fun open() {
        open = true
        if (armed) {
            clock = 0
            flash = true
            sound = true
        }
    }

    fun tick() {
        clock += 1
        when {
            clock == 2L && closed && locked && !armed -> armed = true
            clock == 3L && sound -> sound = false
            clock == 30L && flash -> flash = false
        }
    }

    fun forceArmed() {
        armed = true
        sound = true
        flash = true
    }
    fun forceSound() {
        sound = true
    }
    fun forceFlash() {
        flash = true
    }
}

fun cas(modifier: CarAlarmSystem.() -> Unit) = CarAlarmSystem().apply { modifier() }
