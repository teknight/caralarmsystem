import com.winterbe.expekt.should
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

object CarAlarmSystemTests: Spek({
    Feature("Car Alarm System") {
        Scenario("lock car") {
            When("car is open") {
                cas {
                    open()
                    lock()
                } requiresThat {
                    locked.should.be.`false`
                }
            }
            When("car is closed") {
                cas {
                    lock()
                } requiresThat {
                    locked.should.be.`true`
                }
            }
        }
        Scenario("unlock car") {
            When("car is unarmed") {
                cas {
                    unlock()
                } requiresThat {
                    unlocked.should.be.`true`
                    flash.should.be.`false`
                    sound.should.be.`false`
                    armed.should.be.`false`
                }
            }
            When("car is armed") {
                cas {
                    forceArmed()
                    unlock()
                } requiresThat {
                    unlocked.should.be.`true`
                    flash.should.be.`false`
                    sound.should.be.`false`
                    armed.should.be.`false`
                }
            }
        }
        Scenario("open car") {
            When("armed is false") {
                cas {
                    open()
                } requiresThat {
                    open.should.be.`true`
                    flash.should.be.`false`
                    sound.should.be.`false`
                    armed.should.be.`false`
                }
            }
            When("armed is true") {
                cas {
                    forceArmed()
                    open()
                } requiresThat {
                    open.should.be.`true`
                    flash.should.be.`true`
                    sound.should.be.`true`
                    armed.should.be.`true`
                }
            }
        }
        Scenario("car tick") {
            When("should be armed") {
                cas {
                    lock()
                    repeat(2) { tick() }
                } requiresThat {
                    armed.should.be.`true`
                }
            }
            When("should not be armed") {
                cas {
                    tick()
                } requiresThat {
                    armed.should.be.`false`
                }
            }
            When("should turn sound off") {
                cas {
                    repeat(2) { tick() }
                    forceSound()
                    tick()
                } requiresThat {
                    sound.should.be.`false`
                }
            }
            When("should keep sound on") {
                cas {
                    forceSound()
                    tick()
                } requiresThat {
                    sound.should.be.`true`
                }
            }
            When("should turn flash off") {
                cas {
                    repeat(29) { tick() }
                    forceFlash()
                    tick()
                } requiresThat {
                    flash.should.be.`false`
                }
            }
            When("should keep flash on") {
                cas {
                    forceFlash()
                    tick()
                } requiresThat {
                    flash.should.be.`true`
                }
            }
        }
    }
})