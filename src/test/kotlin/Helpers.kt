infix fun<T> T.requiresThat(req: T.() -> Unit) {
    this.req()
}